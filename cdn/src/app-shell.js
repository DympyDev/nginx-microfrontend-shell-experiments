(function () {
  const template = document.createElement("template");
  template.innerHTML = `
<style>
    .inner-template > header {
        background-color: blue;
        color: white;
    }
</style>
<div class="inner-template">
    <header>This is a header, coming from the app-shell!</header>
    <slot></slot>
</div>`;

  class AppShell extends HTMLElement {
    constructor() {
      super();
      this.attachShadow({ mode: "open" });
      this.shadowRoot.appendChild(template.content.cloneNode(true));
    }
  }

  customElements.define("app-shell", AppShell);
})();
