# NGINX Microfrontend Shell Experiments
Back in 2022, I worked on a number of microfrontends for a national government agency in the Netherlands.
I made most of the server-side changes together with Benoit Schipper from HCS, something he wrote an article about for NGINX:
https://www.nginx.com/blog/hcs-builds-a-flexible-front-end-application-tier-with-nginx/

One of the biggest challenges we faced back then, was the problem of the app-shell being a part of the applications.
In the end we solved this in our front-end code by using a monorepo where every single app was basically being wrapped by this app-shell component.
This introduced the problem where everytime we made a change to the app-shell, we had to release every single application.
At the time this was accepted but it didn't feel as optimized as we would've liked, but we decided to move on.

## A solution with NGINX
During our experiments we also introduced a Content Security Policy (CSP) header, this was also our first experiment using the sub_filter directive from NGINX to set a meta tag in the HTML code we were serving through NGINX.
While the CSP-header itself wasn't all that interesting from a technical standpoint, it did pave the way for other problems we were having where the sub_filter directive could provide a solution, such as the app-shell.

In this repository you will find 2 hypothetical NGINX instances:
- The "web"-instance, this represents the application that we want to wrap in the app-shell
- The "cdn"-instance, this represents a location where the app-shell will be provided from

The "cdn"-instance consists of a basic NGINX server that simply serves an app-shell.js file.
This app-shell.js file contains a single custom-element that accepts slotted content so that it will wrap our application without impacting the functionalities.
It uses a self-invoking function that insures the app-shell will always be bootstrapped and thus is able to be used within our HTML page.

The "web"-instance consists of a single HTML file that represents our application, and which we thus want to be wrapped by our app-shell.
Because this is the more complex configuration of the 2 instances, we use a slightly different NGINX configuration here that makes use of the sub_filter directive to inject the required code as well as wrap the contents of the body with our app-shell custom-element.

Finally, to get this all up-and-running and proof this concept, I've added a simple docker-compose.yaml file that spins up 2 containers using the "cdn"- and "web"-instances.
After spinning up the containers the "cdn"-instance will be hosted on port 2010, while the "web"-instance is hosted on port 2000.

## Next steps
While this is working proof-of-concept, it's just that: A proof-of-concept.
As one of the next-steps, I want to introduce this in a more real-world-scenario.
I'm currently working on setting up a mono-repo using a number of different frameworks, including but not limited to: Angular, React, VueJS and SvelteKit.
The app-shell will be written using StencilJS, a somewhat industry standard web-component framework.
Using this mismatch of different frameworks will hopefully show any shortcomings and the compatibility of this solution with industry standard Web Application frameworks.